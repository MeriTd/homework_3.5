/* 
 Практичні завдання
 2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.

Технічні вимоги:
- Отримати за допомогою модального вікна браузера два числа. Провалідувати отримані значення(перевірити, що отримано числа). Якщо користувач ввів не число, запитувати до тих пір, поки не введе число
- Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /. Провалідувати отримане значення. Якщо користувач ввів не передбачене значення, вивести alert('Такої операції не існує').
- Створити функцію, в яку передати два значення та операцію.
- Вивести у консоль результат виконання функції.
*/


// 2.

let userNumberOne;
let userNumberTwo;

do {
    userNumberOne = prompt('Ведіть перше число');
    userNumberTwo = prompt('Ведіть друге число');
} while (isNaN(userNumberOne) || userNumberOne === null || userNumberOne == " " || userNumberOne == "" && isNaN(userNumberTwo) || userNumberTwo === null || userNumberTwo == " " || userNumberTwo == " ");




let userMathematicOperation = prompt('Ведіть математичну операцію +, -, * або /');
 while (userMathematicOperation !== '+' && userMathematicOperation !== '-' && userMathematicOperation !== '/' && userMathematicOperation !== '*') {
    userMathematicOperation = alert('Такої операції не існує');
    break;
 }

console.log('Ваше перше число', userNumberOne);
console.log('Ваше друге число', userNumberTwo);
console.log('Ваше математична операція', userMathematicOperation);


function calc(userNumberOne, userNumberTwo, userMathematicOperation) {
    switch (userMathematicOperation) {
        case '+': {
           return +userNumberOne + +userNumberTwo;
        }   

        case '-': {
            return +userNumberOne - +userNumberTwo;
        }

        case '/': {
            return +userNumberOne / +userNumberTwo;
        }

        case '*': {
            return +userNumberOne * +userNumberTwo;
        }

        default: { console.log('Ще раз') }
    }
}

let result = calc(userNumberOne, userNumberTwo, userMathematicOperation);
console.log('Відповідь:', result);


