/* 
3. Опціонально. Завдання: 
Реалізувати функцію підрахунку факторіалу числа. 
Технічні вимоги:
- Отримати за допомогою модального вікна браузера число, яке введе користувач.
- За допомогою функції порахувати факторіал числа, яке ввів користувач і вивести його на екран.
- Використовувати синтаксис ES6 для роботи зі змінними та функціями.

*/



let userNumber;

do {
    userNumber = prompt('Введіть число');
} while (isNaN(userNumber) || userNumber === null || userNumber == "" || userNumber == " ");

function factorial(userNumber) {
    return (userNumber != 1) ? userNumber * factorial(userNumber - 1) : 1;
}

alert(factorial(userNumber)); 